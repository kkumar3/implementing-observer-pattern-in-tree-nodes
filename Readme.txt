
Assuming you are in the directory containing this README:

## To clean:
ant -buildfile studentCoursesBackup/src/build.xml clean

-----------------------------------------------------------------------
## To compile: 
ant -buildfile studentCoursesBackup/src/build.xml all

-----------------------------------------------------------------------
## To run by specifying arguments from command line 

ant -buildfile studentCoursesBackup/src/build.xml run -Darg0=input.txt -Darg1=delete.txt -Darg2=output1.txt -Darg3=output2.txt -Darg4=output3.txt

-----------------------------------------------------------------------

## To create tarball for submission

ant -buildfile studentCoursesBackup/src/build.xml tarzip




