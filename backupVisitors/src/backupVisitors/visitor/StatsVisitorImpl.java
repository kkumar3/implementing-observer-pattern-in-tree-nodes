package backupVisitors.visitor;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import backupVisitors.myTree.Node;
import backupVisitors.util.TreeBuilder;

public class StatsVisitorImpl implements TreeVisitorI{


	public ArrayList<Integer> courseCounts = new ArrayList<Integer>();
	private int courseCnt= 0;
	private double studCount = 0;
	public PrintWriter writer;
	private String outputFile;
	
	public StatsVisitorImpl(){
		courseCnt = 0;
		studCount = 0;
	}
	
	public void setFileProp(String file){
		this.outputFile = file;
	}
	
	public void writeTofile(String f1){
		
		
	}
	
	public void visitNode(Node root){
		if(root!=null){
			visitNode(root.leftBranch);
		
			for(int i=0; i<root.courses.size();i++){
				if(root.courses.get(i) != "S"){
					courseCnt++;
				}
			}
			courseCounts.add(courseCnt);
			courseCnt = 0;
			studCount++;

			visitNode(root.rightBranch);
		}
	}

	public void visit(TreeBuilder t1) {
		
		visitNode(t1.root);
		double courseSum = 0;
		int index =0;
		double median = 0;
		
		//FileProcessor f1 = new FileProcessor();
		Collections.sort(courseCounts);
		for(int i = 0; i<courseCounts.size(); i++){
			courseSum += courseCounts.get(i); 
			
		}
		
		double average = courseSum/studCount;
		
	
		
		
		if(courseCounts.size()%2 != 0){
			index = (courseCounts.size()+1)/2;
			
			median = courseCounts.get(index-1);
			
		}
		else{
			
			index = courseCounts.size()/2;
			
			median = (courseCounts.get(index-1)+courseCounts.get(index))/2;
			
		}
		
		try {
				writer = new PrintWriter(outputFile, "UTF-8");
			} catch (FileNotFoundException e) {
				System.err.println("The output file was not found. Please provide an output file");
				e.printStackTrace();
				System.exit(1);
			} catch (UnsupportedEncodingException e) {
				System.err.println("The encoding UTF 8 is not supported with this output file");
				e.printStackTrace();
				System.exit(1);
			}
			
			writer.println("The average is "+average);
			writer.println("The median is "+median);
				
			writer.close();
	}
}
