package backupVisitors.visitor;

import backupVisitors.myTree.Node;
import backupVisitors.util.TreeBuilder;



public class FullTimeStatusVisitorImpl implements TreeVisitorI{
	
	public void visitNode(Node root){
		if(root!=null){
			visitNode(root.leftBranch);
		
			if(root.courses.size() <3){
				root.courses.add("S");
				root.notifyObservers(root.courses);
			}
		
			visitNode(root.rightBranch);
		}
	}
	

	public void visit(TreeBuilder t1) {
		
		visitNode(t1.root);		
		
	}

}
