package backupVisitors.visitor;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import backupVisitors.myTree.Node;
import backupVisitors.util.TreeBuilder;

public class IdenticalRecordsVisitor implements TreeVisitorI{

	public ArrayList<Node> identicalRecords = new ArrayList<Node>();
	public boolean recordFound = false;
	public PrintWriter writer;
	private String outputFile;
	
	public void setFileProp(String file){
		this.outputFile = file;
	}
	
	public void scanNode(Node root, Node refRoot){
		if(root!=null){
			scanNode(root.leftBranch, refRoot);
			Collections.sort(root.courses);
	
			if(root.courses.equals(refRoot.courses) && root.bNo != refRoot.bNo){
					identicalRecords.add(root);
					recordFound = true;
			}
				
			scanNode(root.rightBranch, refRoot);
		}
	}
	
	public void visitNode(Node root, TreeBuilder t){
		if(root!=null){
			visitNode(root.leftBranch, t);
			
			Collections.sort(root.courses);
			scanNode(t.root, root);
			if(recordFound){
				identicalRecords.add(root);
			}
			recordFound = false;
			
		
			visitNode(root.rightBranch, t);
		}
	}
	
	public void visit(TreeBuilder t1) {
		//traverse the tree, compare the courses with every other node of the tree
		
		visitNode(t1.root, t1);
		
		try {
			writer = new PrintWriter(outputFile, "UTF-8");
		} catch (FileNotFoundException e) {
			System.err.println("The output file was not found. Please provide an output file");
			e.printStackTrace();
			System.exit(1);
		} catch (UnsupportedEncodingException e) {
			System.err.println("The encoding UTF 8 is not dupported with this output file");
			e.printStackTrace();
			System.exit(1);
		}
		
		for(int i=0; i< identicalRecords.size() ; i++ ){
			writer.print(identicalRecords.get(i).bNo);
			
			
			for(int k=0; k<identicalRecords.get(i).courses.size(); k++){
				writer.print(identicalRecords.get(i).courses.get(k));
				
			}
			writer.println();
		}
		
		writer.close();
	}

}
