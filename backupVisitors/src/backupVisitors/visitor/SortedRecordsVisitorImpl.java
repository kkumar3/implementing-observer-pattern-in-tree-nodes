package backupVisitors.visitor;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import backupVisitors.myTree.Node;
import backupVisitors.util.NodeComparator;
import backupVisitors.util.TreeBuilder;

public class SortedRecordsVisitorImpl implements TreeVisitorI{
	
	public ArrayList<Node> allRecords = new ArrayList<Node>();
	private String outputFile;
	public PrintWriter writer;
	public void setFileProp(String file){
		this.outputFile = file;
	}
	
	public void visitNode(Node root){
		if(root!=null){
			visitNode(root.leftBranch);
		
			allRecords.add(root);
		
			visitNode(root.rightBranch);
		}
	}
	
	public void visit(TreeBuilder t1) {
		
		visitNode(t1.root);
		Collections.sort(allRecords,new NodeComparator());
		
		
		try {
			writer = new PrintWriter(outputFile, "UTF-8");
		} catch (FileNotFoundException e) {
			System.err.println("The output file was not found. Please provide an output file");
			e.printStackTrace();
			System.exit(1);
		} catch (UnsupportedEncodingException e) {
			System.err.println("The encoding UTF 8 is not dupported with this output file");
			e.printStackTrace();
			System.exit(1);
		}
		
		for(int i=0; i< allRecords.size() ; i++ ){
			writer.print(allRecords.get(i).bNo);
			
		
			for(int k=0; k<allRecords.get(i).courses.size(); k++){
				writer.print(allRecords.get(i).courses.get(k));
				
			}
			writer.println();
		}
		
		writer.close();
	}

	
	
}
