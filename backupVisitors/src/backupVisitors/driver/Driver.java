 package backupVisitors.driver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import backupVisitors.util.Results;
import backupVisitors.util.TreeBuilder;
import backupVisitors.visitor.FullTimeStatusVisitorImpl;
import backupVisitors.visitor.IdenticalRecordsVisitor;
import backupVisitors.visitor.SortedRecordsVisitorImpl;
import backupVisitors.visitor.StatsVisitorImpl;
import backupVisitors.visitor.TreeVisitorI;
import backupVisitors.myTree.Node;

public class Driver {
	
	public static void main(String[] args){
		
		//validating input arguments
		if(args.length != 5){
			System.out.println("Invalid number of arguments provided. Expected is one input file, delete file and 3 output files.");
			System.exit(1);
		}
		
		TreeVisitorI fullTimeStatus = new FullTimeStatusVisitorImpl();
		TreeVisitorI identicalRecords = new IdenticalRecordsVisitor();
		TreeVisitorI sortedRecords = new SortedRecordsVisitorImpl(); 
		TreeVisitorI stats = new StatsVisitorImpl();
		
		//initialization of three trees
		TreeBuilder orig_tree = new TreeBuilder();
		TreeBuilder back_1_tree = new TreeBuilder();
		TreeBuilder back_2_tree = new TreeBuilder();
		
		//reference variables
		Node orig_node; 
		Node back_1 = null;
		Node back_2 = null;
		
		int bno = 0;
		String courseIn = "";
		String inputLine = "";
		
		//instantiation of result class variables
		/*Results rOrig = new Results(args[2]);
		Results rBack1 = new Results(args[3]);
		Results rBack2 = new Results(args[4]);
		*/
		//read and insert values based on input.txt
		try{
			
			
			File f = new File(args[0]);
			BufferedReader b = new BufferedReader(new FileReader(f));
			
			//read lines one by 1
			while ((inputLine = b.readLine())!= null) {
				
					bno = Integer.parseInt(inputLine.substring(0,4));
					courseIn = inputLine.substring(5);
					
					//search tree, if the bno already exists
					//if yes, then modify the existing node and notify observers.
					if(orig_tree.search(bno)!= null){
						Node ref_node = orig_tree.search(bno);
						
						ref_node.courses.add(courseIn);
						ref_node.notifyObservers(ref_node.courses);
					}
					//if not proceed to creating nodes and add them to trees
					else{
						//create a node
						orig_node = new Node(bno,courseIn);
						
						//clone node 
						try {
							back_1 = (Node)orig_node.clone();
							back_2 = (Node)orig_node.clone();
						} catch (CloneNotSupportedException e) {
							System.err.println("cloning is not supported for this object type");
							e.printStackTrace();
							System.exit(0);
						}
						
						//add references of the cloned nodes to original node
						orig_node.listeners.add(back_1);
						
						orig_node.listeners.add(back_2);
						
						//add nodes to the respective trees
						orig_tree.insertNode(orig_node);
						back_1_tree.insertNode(back_1);
						back_2_tree.insertNode(back_2);
					}
					
			}
			
			
		} catch (IOException e) {
			System.err.println("There is no input file present");
			e.printStackTrace();
			System.exit(1);
		} 
		
		try{
			
			File f1 = new File(args[1]);
		
			BufferedReader b = new BufferedReader(new FileReader(f1));
			
			//read delete.txt line by line
			while ((inputLine = b.readLine())!= null) {
				
				bno = Integer.parseInt(inputLine.substring(0,4));
				courseIn = inputLine.substring(5);
				
				//search if the bno provided exists in tree
				if(orig_tree.search(bno)!= null){
					//get the node
					Node ref_del_node = orig_tree.search(bno);
					//modify orig_tree node courses
					orig_tree.delete(ref_del_node, courseIn);
					//call notify observers to update their respective contents
					ref_del_node.notifyObservers(ref_del_node.courses);
				}
			}
			
		
		} catch (IOException e) {
			System.err.println("There is no input file present");
			e.printStackTrace();
			System.exit(1);
		} 
		
		//call printNode on instances of Results class and respective trees
		/*
		orig_tree.printNodes(rOrig, orig_tree.root);
		back_1_tree.printNodes(rBack1, back_1_tree.root);
		back_2_tree.printNodes(rBack2, back_2_tree.root);
		
		//store the results in output file
		rOrig.writeToFile();
		rBack1.writeToFile();
		rBack2.writeToFile();
		*/
	
		((StatsVisitorImpl) stats).setFileProp(args[2]);
		((SortedRecordsVisitorImpl) sortedRecords).setFileProp(args[3]);
		((IdenticalRecordsVisitor) identicalRecords).setFileProp(args[4]);
		//
		orig_tree.accept(fullTimeStatus);
		back_1_tree.accept(stats);
		back_2_tree.accept(sortedRecords);
		orig_tree.accept(identicalRecords);
		
	
		
		
	}
}	
	
