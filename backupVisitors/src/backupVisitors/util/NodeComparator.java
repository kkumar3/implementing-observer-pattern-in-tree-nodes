package backupVisitors.util;
import java.util.Comparator;

import backupVisitors.myTree.Node;

public class NodeComparator implements Comparator<Node> {
	
	   
		public int compare(Node n1, Node n2) {
	        if(n1.getBno() < n2.getBno()){
	            return 1;
	        } else {
	            return -1;
	        }
	    }
	}

