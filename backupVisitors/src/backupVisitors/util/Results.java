package backupVisitors.util;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import backupVisitors.myTree.Node;

public class Results implements FileDisplayInterface, StdoutDisplayInterface{
	
	public PrintWriter writer;
	//Node array list to store results
	public ArrayList<Node> resultCourse= new ArrayList<Node>();
	//constructor to initialize Results instance with an output file
	public Results(String f) {
		 try {
			writer = new PrintWriter(f, "UTF-8");
		} catch (FileNotFoundException e) {
			System.err.println("The output file was not found. Please provide an output file");
			e.printStackTrace();
			System.exit(1);
		} catch (UnsupportedEncodingException e) {
			System.err.println("The encoding UTF 8 is not dupported with this output file");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	//function to write results in output files.
	public void writeToFile(){
		
		for(int i=0; i< resultCourse.size() ; i++ ){
			writer.print(resultCourse.get(i).bNo);
			
			System.out.println(resultCourse.get(i).bNo);
			for(int k=0; k<resultCourse.get(i).courses.size(); k++){
				writer.print(resultCourse.get(i).courses.get(k));
				
			}
			writer.println();
		}
		
		writer.close();
	}
	
	public void writeToStdout(){
		
		for(int i=0; i< resultCourse.size(); i++ ){
			System.out.print(resultCourse.get(i).bNo);
			for(int k=0; k<resultCourse.get(i).courses.size(); i++){
				System.out.print(resultCourse.get(i).courses.get(k));
			}
			System.out.println();
		}
	}
	
		
}
