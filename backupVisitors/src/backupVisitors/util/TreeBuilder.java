package backupVisitors.util;

import backupVisitors.myTree.Node;
import backupVisitors.visitor.TreeVisitorI;

//I have taken reference from this source for implementing basic BST tree
//http://algorithms.tutorialhorizon.com/binary-search-tree-complete-implementation/

public class TreeBuilder implements TreeVisitorI {
	
	//root node 
	public Node root;
	
	//constructor
	public TreeBuilder(){
		this.root=null;
	}
	
	//function to insert new nodes in the tree
	public void insertNode(Node n1){
		
		
		Node current = root;
		Node parent = null;
		
		if(root==null){
			root = n1;
			return;
		}
		
		while(true){
			
			parent = current;
			if(n1.bNo < current.bNo){
				
				current = current.leftBranch;
				if(current == null){
					parent.leftBranch = n1;
					return;
				}
			}
			else{
				
				current = current.rightBranch;
				if(current == null){
					parent.rightBranch = n1;
					return;
				}
			}
		}	
	}
	
	//function to delete course from the tree node
	public boolean delete(Node n, String cIn){	
		
		if(n.courses.indexOf(cIn) != -1){
			int k= n.courses.indexOf(cIn);
			n.courses.remove(k);
			// it always returns true because, the tree 
			//is searched for element before performing delete operation
		
		}
		return true;
	}
	
	//function to search for a specific node
	public Node search(int bNoIn){
		
		Node current = root;
		while(current!=null){
			if(current.bNo==bNoIn){
				//if found, it returns a ref to the node itself
				return current;
			}else if(current.bNo>bNoIn){
				current = current.leftBranch;
			}else{
				current = current.rightBranch;
			}
		}
		return null;
	}
	

	//function to store the nodes into Result class instance 
	public void printNodes(Results r, Node root){
		if(root!=null){
			printNodes(r, root.leftBranch);
			
			r.resultCourse.add(root);
			
			System.out.println();
			printNodes(r, root.rightBranch);
			
		}
	}	
	
	
	public void accept(TreeVisitorI v1){
		v1.visit(this);
	}
	
	public void visit(TreeBuilder t1){
	
	}
	 
}
