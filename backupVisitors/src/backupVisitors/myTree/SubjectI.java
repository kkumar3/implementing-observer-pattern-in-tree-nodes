package backupVisitors.myTree;

import java.util.ArrayList;

public interface SubjectI {

	public void notifyObservers(ArrayList<String> cIn);
}
