package backupVisitors.myTree;

import java.util.ArrayList;

public class Node implements ObserverI, SubjectI, Cloneable{
	
	//data members
	public int bNo;
	public ArrayList<Node> listeners = new ArrayList<Node>();
	public ArrayList<String> courses = new ArrayList<String>();
	public Node leftBranch;
	public Node rightBranch;

	//constructor
	public Node(int bNoIn, String courseIn){
		this.bNo = bNoIn;
		this.courses.add(courseIn);
	}
	
	//interface methods
	public void notifyObservers(ArrayList<String> cIn){
		
		listeners.get(0).update(cIn);
		listeners.get(1).update(cIn);
	
	}
	
	public void update(ArrayList<String> cIn){
		
		this.courses = cIn;	
	}
	
	//clone method
	public Object clone() throws CloneNotSupportedException{

		return (Node)super.clone();
		
	}

	public int getBno() {
		
		return bNo;
	}
	
	/*public boolean equals(Object otherObject)
	{
	    if (otherObject instanceof Node)
	    {
	        Node ocounter = (Node) otherObject;
	        if (this.courses != ocounter.courses)
	            return false;
	        
	    } else {
	        return false;
	    }
	    return true;
	}*/
}
